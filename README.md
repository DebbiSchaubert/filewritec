====================Application======================

This is an easy and uses the least amount of code, because I am working with files a lot in my project.Simple code to read and write strings. In .net, file handling is provided by the framework, not the languages there are no C# keywords to declare and manipulate files for instance.
Debbi Schaubert - Software developer at https://monstermove.uk/services/self-storage-hull/

CODE :

This defines the extension method on the string type

using System.IO;//File, Directory, Path

namespace Lib
{
    /// <summary>
    /// Handy string methods
    /// </summary>
    public static class Strings
    {
        /// <summary>
        /// Extension method to write the string Str to a file
        /// </summary>
        /// <param name="Str"></param>
        /// <param name="Filename"></param>
        public static void WriteToFile(this string Str, string Filename)
        {
            File.WriteAllText(Filename, Str);
            return;
        }

        // of course you could add other useful string methods...
    }//end class
}//end ns

![how-to-write-text-file-csharp-02.jpg](https://bitbucket.org/repo/rGj7e8/images/2234130370-how-to-write-text-file-csharp-02.jpg)